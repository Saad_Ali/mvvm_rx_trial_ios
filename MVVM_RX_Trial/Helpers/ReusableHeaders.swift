//
//  ReusableHeaders.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation

struct ReusableHeaders : Encodable {
    var Authorization = ""
    
    init() {
        // put your authorization code here
        self.Authorization = ""
    }
    
}
