//
//  UIHelper.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation
import TWMessageBarManager
import SVProgressHUD
import Reachability

class UIHelper {
    class func showErrorMessage(_ errorMessage: String?, title: String?) {
        if !TWMessageBarManager.sharedInstance().isMessageVisible {
            
            TWMessageBarManager.sharedInstance().showMessage(withTitle: title, description: errorMessage, type: .error, statusBarStyle: UIStatusBarStyle.lightContent, callback: nil)
        }
    }
    class func showSuccessMessage(_ message: String?, title: String?) {
        if !TWMessageBarManager.sharedInstance().isMessageVisible {
            TWMessageBarManager.sharedInstance().showMessage(withTitle: title, description: message, type: .success, statusBarStyle: UIStatusBarStyle.lightContent, callback: nil)
        }
    }
    class func showProgressBar() {
        let reachability = Reachability()
        if reachability?.connection.description == .none {
            //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error Connection" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil ];
            //   [alert show];
        } else {
            SVProgressHUD().defaultAnimationType = .flat
//            SVProgressHUD.setForegroundColor(UIColor(rgb: 10, green: 180, blue: 228, alpha: 1.0))
            SVProgressHUD.show()
        }
    }
    
    class func showProgressBarWithDimView() {
        SVProgressHUD().defaultMaskType = .black
        SVProgressHUD().defaultAnimationType = .flat
//        SVProgressHUD.setForegroundColor(UIColor(rgb: 10, green: 180, blue: 228, alpha: 1.0))
        SVProgressHUD.show()
    }
    class func dissmissProgressBar() {
        SVProgressHUD.dismiss()
    }
    class func loginButtonLabelColor() -> [NSAttributedString.Key : NSObject?]? {
        var _fontSize: Float
//        if DeviceHelper.IS_IPAD() {
//            _fontSize = 16.0
//        } else {
            _fontSize = 14.0
//        }
        var attrDict: [NSAttributedString.Key : NSObject?]? = nil
        
        if let aSize = UIFont(name: "HelveticaNeue-Light", size: CGFloat(_fontSize)) {
            attrDict = [NSAttributedString.Key.font: aSize, NSAttributedString.Key.foregroundColor: UIColor.black ]
        }
        return attrDict
    }
    class func loginRegisterationLableColor()->[NSAttributedString.Key : Any]?
    {
        var fontSize = 0.0
//        if DeviceHelper.IS_IPAD(){
//            fontSize = 16.0
//        }
//        else{
            fontSize = 14.0
//        }
        var attrDict: [NSAttributedString.Key : Any]? = nil
        if let aSize = UIFont(name: "HelveticaNeue-Light", size: CGFloat(fontSize)) {
            attrDict = [NSAttributedString.Key.font: aSize,NSAttributedString.Key.foregroundColor : UIColor.black]
        }
        return attrDict
    }
    class func loginButtonLabelColor()->[NSAttributedString.Key : Any]?{
        var fontSize = 0.0
//        if DeviceHelper.IS_IPAD() {
//            fontSize = 16.0
//        }
//        else{
            fontSize = 14.0
//        }
        var attrDict: [NSAttributedString.Key : Any]? = nil
//        if let aSize = UIFont(name: "HelveticaNeue-Light", size: CGFloat(fontSize)) {
//            attrDict = [NSAttributedStringKey.font: aSize,NSAttributedStringKey.foregroundColor : UIColor(fromHexString: LoginModuleConstants.loginButtonLabelColor)]
//        }
        return attrDict
        
    }
    
    
    class func addShadow(_ view: UIView?) {
        view?.layer.masksToBounds = false
        view?.layer.shadowColor = UIColor.gray.cgColor
        view?.layer.shadowOpacity = 0.4
        view?.layer.shadowRadius = 2
    }
    
    class func calculateHeight(forText text: String?, havingWidth widthValue: CGFloat, andFont fontDictionary: [AnyHashable : Any]?) -> CGSize {
        var size = CGSize.zero
        if text != nil {
            let frame: CGRect? = text?.boundingRect(with: CGSize(width: widthValue, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: fontDictionary as? [NSAttributedString.Key : Any], context: nil)
            size = CGSize(width: frame?.size.width ?? 0.0, height: (frame?.size.height ?? 0.0) + 1)
        }
        return size
    }
    
    
}

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
