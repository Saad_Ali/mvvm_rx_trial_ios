//
//  SwiftTypesExtentions.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation

// MARK: - extension to provide default value to optional string
extension Optional where Wrapped == String {
    func defaultValueIfNotExists()->String{
        return self != nil ? self! : ""
    }
}

// MARK: - extention to convert struct to json dictionary to send to api
extension Encodable {
    subscript(key: String) -> Any? {
        return dictionary[key]
    }
    var data: Data {
        return try! JSONEncoder().encode(self)
    }
    var dictionary: [String: Any] {
        return (try? JSONSerialization.jsonObject(with: data)) as? [String: Any] ?? [:]
    }
}
