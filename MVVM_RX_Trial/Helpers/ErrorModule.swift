//
//  ErrorModule.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation
import ObjectMapper

struct ErrorModule : Error{
    let desc:String
    let code:Int
}
