//
//  ProductsConstants.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation

class ProductsConstants: NSObject {
    
    private static let BASE_URL = "" // put you host ip here.
    private static let productsPath = "customer/getProducts/"
    public static let productsUrl = ProductsConstants.BASE_URL + ProductsConstants.productsPath
    
    
}
