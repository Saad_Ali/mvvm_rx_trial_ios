//
//  ProductsViewModel.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Alamofire
import RealmSwift

class ProductsViewModel: NSObject {
    
    private var repo = ProductsRepository<ProductsResponseModel,ProductsResponseModel>()
    private var disposeBag = DisposeBag()
    var id:String = ""
    public private(set) var resultProductsObjs = PublishSubject<ProductsListModel>()
    
    
    private func initializeSubscribers()
    {
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        // product id that you can get it from the products list
        id = ""
        repo.id = self.id
        repo.objObservableRemoteData.asObservable().subscribe(onNext: { (productsList) in
            
            productsList.id = self.id
            self.resultProductsObjs.onNext(productsList.data!)
            print(productsList)
        }, onError: { (err) in
            print(err)
        }, onCompleted: {
            //
            print("completed")
        }).disposed(by: disposeBag)
        
    }
    
    override init() {
        super.init()
        initializeSubscribers()
    }
    
    func getProductsListData()
    {
        
        repo.getProductsData(url: ProductsConstants.productsUrl, data: nil, headers: ReusableHeaders.init().dictionary as! HTTPHeaders
        )
        
    }
}
