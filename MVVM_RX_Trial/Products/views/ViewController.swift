//
//  ViewController.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import UIKit
import RxSwift


class ViewController: UIViewController {
    
    
    var productsViewModel = ProductsViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.listenToProductsList()
        self.productsViewModel.getProductsListData()
    }

    fileprivate func listenToProductsList()
    {
        productsViewModel.resultProductsObjs.subscribe(onNext: { (productsList) in
            UIHelper.dissmissProgressBar()
            print(productsList)
            
        }, onError: { (_) in
            UIHelper.dissmissProgressBar()
        }, onCompleted: {
            UIHelper.dissmissProgressBar()
            
        }).disposed(by: disposeBag)
    }
    
}

