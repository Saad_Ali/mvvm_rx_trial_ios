//
//  ProductsResponseModel.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import ObjectMapper_Realm

class ProductsResponseModel: Object, Mappable {
    
    @objc dynamic var statusCode:Int = 0
    @objc dynamic var message:String = ""
    @objc dynamic var data:ProductsListModel?
    @objc dynamic var id:String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        statusCode                  <- map["statusCode"]
        message                        <- map["message"]
        data                           <- map["data"]
    }
    override class func primaryKey() -> String? {
        return "id";
    }
}
