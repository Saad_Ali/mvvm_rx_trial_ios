//
//  ProductsRequestClass.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ObjectMapper
import RxRealm
import Alamofire

class ProductsRequestClass<U:Mappable>: ProductsDataRemoteSource {
    
    typealias T = U
    
    func callApi(url:String ,params : Parameters?, headers:HTTPHeaders?)->Observable<U>? {
        return  Observable.create{
            observer in
            Alamofire.request(url,
                              method: .get,
                              parameters: params, encoding: JSONEncoding.default,headers:headers
                )
                .responseJSON { response in
                    switch response.result {
                    case .success(let json):
                        guard let value = json as? [String: Any]
                            else {
                                return
                        }
                        let responseObj = U(JSON: value)
                        observer.onNext(responseObj!)
                    case .failure(let error):
                        observer.onError(error)
                    }
            }
            return Disposables.create {
            }
        }
    }
}
