//
//  ProductModel.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import ObjectMapper_Realm

class ProductModel: Object, Mappable {
    
    @objc dynamic var id:String = ""
    @objc dynamic var updatedAt:String = ""
    @objc dynamic var createdAt:String = ""
    @objc dynamic var categoryId:String = ""
    @objc dynamic var brand:String = ""
    @objc dynamic var arbicName:String = ""
    
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id                              <- map["id"]
        updatedAt                      <- map["updatedAt"]
        createdAt                        <- map["createdAt"]
        categoryId                       <- map["categoryId"]
        brand                           <- map["brand"]
        arbicName                    <- map["arbicName"]
    }
    override class func primaryKey() -> String? {
        return "id";
    }
}

