//
//  ProductsList.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import ObjectMapper_Realm

class ProductsListModel: Object, Mappable {
    
    @objc dynamic var id:String = ""
    var ProductsList:List<ProductModel> = List<ProductModel>()
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        ProductsList               <- (map["products"], ListTransform<ProductModel>())
    }
    override class func primaryKey() -> String? {
        return "id";
    }
}
