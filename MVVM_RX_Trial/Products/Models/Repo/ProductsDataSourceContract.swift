//
//  ProductsDataSourceContract.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ObjectMapper
import RealmSwift
import Alamofire


// the 2 main protocols used for the repo one is which call remote and the other used for local db
// and the repo conform to these protocols
// and the LoginRequestClass and userDao conform to it and make implementation to it

typealias ProductsDataSourceContract = ProductsDataRemoteSource & ProductsDataLocalSource


protocol ProductsDataRemoteSource {
    
    associatedtype T:Mappable
    
    func callApi(url:String ,params : Parameters?,headers:HTTPHeaders?) -> Observable<T>?
    
}


protocol ProductsDataLocalSource {
    
    associatedtype U:Object
    
    func fetch()->U?
    
    func fetch(withId:String)->U?
    
    func fetchArray()->[U]?
    
    func insert(productsDataModel : U)
        
    func delete()
}
