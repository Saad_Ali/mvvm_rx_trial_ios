//
//  ProductsRepo.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation
import Foundation
import RxSwift
import RxCocoa
import ObjectMapper
import RealmSwift
import Alamofire


class ProductsRepository<REMOTE:Mappable,LOCAL:Object>:
ProductsDataSourceContract {
    
    typealias T = REMOTE
    
    private let objProductsRequestClass:ProductsRequestClass = ProductsRequestClass<REMOTE>()
    private let objProductsDao:ProductsDao = ProductsDao<LOCAL>()
    
    private var objObservableDao = PublishSubject<LOCAL>()
    private var objObservableRemote = PublishSubject<REMOTE>()
    
    public var errorModule = PublishSubject<ErrorModule>()
    
    private var bag = DisposeBag()
    
    var id:String = ""
    // getter observable
    
    public var objObservableLocal:PublishSubject<LOCAL>{
        return objObservableDao
    }
    
    public var objObservableRemoteData:PublishSubject<REMOTE>{
        return objObservableRemote
    }

    func getProductsData(url:String, data:Parameters?, headers:HTTPHeaders, bool:Bool = true)
    {
        if bool{
            if let cashedData = self.fetch(withId: self.id)
            {
                objObservableDao.onNext(cashedData)
                
                self.callBackEndApi(url: url, params: data, headers: headers)
            }
            else{
                self.callBackEndApi(url: url, params: data, headers: headers)
            }
            
        }
        else
        {
            self.callBackEndApi(url: url, params: data, headers: headers)
        }
    }
    
    func callBackEndApi(url: String, params: Parameters?, headers: HTTPHeaders)
    {
        self.callApi(url: url, params: params, headers: headers)?.subscribe({ (subObj) in
            
            switch subObj
            {
            case .next(let responseObj):
                self.objObservableRemote.onNext(responseObj)
                self.insert(productsDataModel: responseObj as! LOCAL)
                self.objObservableDao.onNext(responseObj as! LOCAL)
            case .error(_):
                if let resultData = self.fetch(withId: self.id)
                {
                    self.objObservableDao.onNext(resultData)
                }
                else{
                    
                    self.errorModule.onNext(ErrorModule(desc: "network first time fail", code: 404))
                }
            case .completed:
                print("Completed")
            default:
                break
            }
            
        }).disposed(by: bag)

    }
    
    func callApi(url: String, params: Parameters?, headers: HTTPHeaders?) -> Observable<REMOTE>? {
        if let objObserve = objProductsRequestClass.callApi(url: url,   params: params, headers: headers)
        {
            return objObserve
        }
        return Observable.empty()
    }
}
extension ProductsRepository{
    typealias U = LOCAL
    
    func fetch() -> LOCAL? {
        return objProductsDao.fetch()
    }
    
    func fetch(withId : String) -> LOCAL? {
        return objProductsDao.fetch(withId: withId)
    }
    
    func fetchArray() -> [LOCAL]? {
        //
        return objProductsDao.fetchArray()
    }
    
    func insert(productsDataModel : LOCAL) {
        objProductsDao.insert(productsDataModel: productsDataModel)
    }
    
    
    func delete() {
        objProductsDao.delete()
    }
    
}
