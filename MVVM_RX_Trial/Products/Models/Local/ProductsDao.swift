//
//  ProductsDao.swift
//  MVVM_RX_Trial
//
//  Created by Saad Abou El Sooud on 3/7/19.
//  Copyright © 2019 Saad Abou El Sooud. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class ProductsDao<R:Object>: ProductsDataLocalSource{
    
    func insert(productsDataModel : R) {
        do {
            let realm = try Realm()
            realm.beginWrite()
            realm.add(productsDataModel,update:true)
            try realm.commitWrite()
        } catch (let error) {
            print(error)
        }
    }
    
    func fetch()->R? {
        do {
            let realm = try  Realm()
            return realm.objects(R.self).first
        } catch (let error) {
            print(error)
            return nil
        }
    }
    
    func fetch(withId:String)->R? {
        do {
            let realm = try  Realm()
            return realm.objects(R.self).filter("id=%@", withId).first
            
        } catch (let error) {
            print(error)
            return nil
        }
    }
    
    func fetchArray()->[R]? {
//        do {
//            let realm = try  Realm()
//            return realm.objects(R.self).itemsResltToArray()
//        } catch (let error) {
//            print(error)
            return nil
//        }
    }
    
    func delete(){
        do {
            let realm = try Realm()
            realm.beginWrite()
            realm.delete(realm.objects(R.self))
            try realm.commitWrite()
        } catch (let error) {
            print(error)
        }
    }
}
